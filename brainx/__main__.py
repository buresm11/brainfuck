#!/usr/bin/python

import sys

import modules.brainfuck
import modules.getch

def main():

    from optparse import OptionParser
    parser = OptionParser(usage='usage: %prog FILE', version="%prog 1.0")
    (options, args) = parser.parse_args()

    try:
        with open(args[0], mode='r') as f:
            code = f.read()
    except:
        code = args[0]

    x = modules.brainfuck.BrainFuck(code)


if __name__ == "__main__": main()