import zlib

class PNGWrongHeaderError(Exception):
    """Exception that says not a png file"""
    pass

class PNGNotImplementedError(Exception):
    """Exception that says cannot do"""
    pass

class Png():
    """Reading png pictures"""
    
    def __init__(self,filepath):
        self.colors = []

        with open(filepath,mode='rb') as pngfile:
            self.pngBinData = pngfile.read()


        self._headerCheck()
        self._getChunks()
        self._getSize()
        self._processPixels()


    def _headerCheck(self):
        """Check if header is really png"""

        if self.pngBinData[:8] != b'\x89PNG\r\n\x1a\n':
            raise PNGWrongHeaderError()
        self.pngBinData = self.pngBinData[8:]
        return self

    def _bytesToNumber(self,bytes):
        """Convert some bytes to one number"""

        n=0
        for b in bytes:
            n = n*256+b
        return b

    def _getChunks(self):
        """get chunk from binary data"""

        self.chunks = []

        i=0
        while i < len(self.pngBinData):
            l = self._bytesToNumber(self.pngBinData[i:i+4])
            i += 4 #skip lengh in byte stream

            self.chunks += [{'type':self.pngBinData[i:i+4], 'data':self.pngBinData[i+4:i+l+4]}]

            i += l+8 # skipping type, actual data, crc

        del self.pngBinData
        return self

    def _concatenationChunks(self):
        """concatenation of all chunks and decompress it"""
        idat = b''
        for chunk in self.chunks:
            if chunk['type'] == b'IDAT':
                idat += chunk['data']

        return zlib.decompress(idat)

    def _getSize(self):
        for chunk in self.chunks:
            if(chunk['type'] == b'IHDR'):
                ihdr = chunk['data']

        self.width = self._bytesToNumber(ihdr[0:4])
        self.height = self._bytesToNumber(ihdr[4:8])

        if self._bytesToNumber(ihdr[8:9]) != 8:  # 8 bits depth
            raise PNGNotImplementedError()
        if self._bytesToNumber(ihdr[9:10]) != 2: # allowing only Truecolour (Each pixel is an R,G,B triple)
            raise PNGNotImplementedError()
        if self._bytesToNumber(ihdr[10:11]) != 0: 
            raise PNGNotImplementedError()
        if self._bytesToNumber(ihdr[11:12]) != 0:
            raise PNGNotImplementedError()
        if self._bytesToNumber(ihdr[12:13]) != 0:
            raise PNGNotImplementedError()

        return self


    def _processPixels(self):
        sel


if __name__ == '__main__':
    Png('test_data/sachovnice_paleta.png')

