import sys

import modules.getch

class BrainFuck:
    """BrainFuck"""
    def __init__(self,code):
        """Brainfuck initialization"""

        self.memory=bytearray(100)
        self.memory_size=100
        self.actual_memory_size=1
        self.memory_pointer=0
        self.code_pointer=0
        self.code = code;

        self.input = self._getinput()
        self.output = ''

        self._evaluate()

    def _getinput(self):
        """Input after ! if any"""
        x=0
        while x < len(self.code) and self.code[x] != '!':
            x += 1

        if x+1 < len(self.code):
            input = self.code[x+1:]
            self.code = self.code[:x]
            return input

        return ''

    def _getnextinput(self):
        """get next char from input ! or keybord"""
        if len(self.input) == 0:
            #return sys.stdin.read(1)
            return modules.getch.getch()
        else:
            ret = self.input[0]
            self.input = self.input[1:]
            return ret

    def _processloops(self):
        """set up begin and end of all loops"""
        stack = []
        bracepositions = [0] * len(self.code)

        x=0
        while x < len(self.code):
            if self.code[x] == '[': stack.append(x)
            if self.code[x] == ']':
                begin = stack.pop()
                bracepositions[begin] = x
                bracepositions[x] = begin
            x += 1

        return bracepositions 

    def _evaluate(self):
        """_evaluate brainfuck code"""

        bracepositions = self._processloops()
        while self.code_pointer < len(self.code):
            #move memory right >
            if self.code[self.code_pointer] == '>':
                self.memory_pointer +=1
                #need realloc
                if self.memory_pointer == self.memory_size:
                    self.memory += bytearray(self.memory_size)
                    self.memory_size += self.memory_size
            #move memory left <
            if self.code[self.code_pointer] == '<':
                if self.memory_pointer > 0: 
                    self.memory_pointer -= 1
            #increase memory +        
            if self.code[self.code_pointer] == '+':
                if self.memory[self.memory_pointer] == 255:
                    self.memory[self.memory_pointer] = 0
                else:
                    self.memory[self.memory_pointer] += 1
            #decrease memory -
            if self.code[self.code_pointer] == '-':
                if self.memory[self.memory_pointer] == 0:
                    self.memory[self.memory_pointer] = 255
                else:
                    self.memory[self.memory_pointer] -= 1
            #print memory .
            if self.code[self.code_pointer] == '.':
                print(chr(self.memory[self.memory_pointer]),end='')
                sys.stdout.flush()
                self.output += chr(self.memory[self.memory_pointer])
            #read into memory ,
            if self.code[self.code_pointer] == ',':
                self.memory[self.memory_pointer] = ord(self._getnextinput())
                print("get here too")
            #begin loop
            if self.code[self.code_pointer] == '[' and self.memory[self.memory_pointer] == 0:
                self.code_pointer =  bracepositions[self.code_pointer]
            #end loop
            if self.code[self.code_pointer] == ']' and self.memory[self.memory_pointer] != 0:
                self.code_pointer =  bracepositions[self.code_pointer] 

            self.code_pointer +=1

